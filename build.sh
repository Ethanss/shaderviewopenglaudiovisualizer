outn=${PWD##*/}
if [ -n "$1" ]
then
outn="$outn""$1"" -m""$1"
fi
gcc main.c -o Build/$outn -mmmx -msse -msse2 -msse4.1 -msse4.2 -fdce -fno-strict-aliasing -D_FILE_OFFSET_BITS=64 -Wl,--gc-sections -Wall -Wextra -Wno-format -Wno-incompatible-pointer-types -Wno-discarded-qualifiers -Wno-implicit -Wno-unused-result -Wno-unused-parameter -Wno-missing-braces -Wno-sign-compare -Wl,--no-undefined -lSDL2 -lSDL2_ttf -lassimp -lpng -lz -lGL -lm -lpthread -lXss -ldl -lasound -lpulse-simple -lpulse -lsndio -lX11 -lXext -lXcursor -lXinerama -lXi -lXrandr -lXxf86vm -lwayland-egl -lwayland-client -lwayland-cursor -lxkbcommon -lrt
