﻿/*Copyright 2020 Ethan Alexander Shulman
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#ifdef _MSC_VER
#define inline __inline
#define snprintf _snprintf
#include <malloc.h>
#endif

#define EXCLUDE_LIB_SDL_TTF

#include "../../stream.h"
#include "../../window.h"
#include "../../graphics.h"
#include "../../pnghelper.h"
#include "../../timer.h"
#include "../../json.h"

//#include "../../printftofile.h"

#ifndef __linux__
#include "../../cosinetransform.h"
#include <audioclient.h>
#include <mmdeviceapi.h>

static const CLSID CLSID_MMDeviceEnumerator  = {0xBCDE0395,0xE52F,0x467C,{0x8E,0x3D,0xC4,0x57,0x92,0x91,0x69,0x2E}};
static const IID   IID_IMMDeviceEnumerator   = {
    0xa95664d2, 0x9614, 0x4f35, {0xa7, 0x46, 0xde, 0x8d, 0xb6, 0x36, 0x17, 0xe6}
};
static const IID   IID_IMMNotificationClient = {
    0x7991eec9, 0x7e89, 0x4d85, {0x83, 0x90, 0x6c, 0x70, 0x3c, 0xec, 0x60, 0xc0}
};
static const IID   IID_IAudioClient = {
    0x1cb9ad4c, 0xdbfa, 0x4c32, {0xb1, 0x78, 0xc2, 0xf5, 0x68, 0xa7, 0x03, 0xb2}
};
static const IID   IID_IAudioRenderClient    = {
    0xf294acfc, 0x3146, 0x4483, {0xa7, 0xbf, 0xad, 0xdc, 0xa7, 0xc2, 0x60, 0xe2}
};
static const IID   IID_IAudioSessionControl  = {
    0xf4b1a599, 0x7266, 0x4319, {0xa8, 0xca, 0xe7, 0x0a, 0xcb, 0x11, 0xe8, 0xcd}
};
static const IID   IID_IAudioSessionEvents   = {
    0x24918acc, 0x64b3, 0x37c1, {0x8c, 0xa9, 0x74, 0xa6, 0x6e, 0x99, 0x57, 0xa8}
};
static const IID IID_IMMEndpoint = {
    0x1be09788, 0x6894, 0x4089, {0x85, 0x86, 0x9a, 0x2a, 0x6c, 0x26, 0x5a, 0xc5}
};
static const IID IID_IAudioClockAdjustment = {
    0xf6e4c0a0, 0x46d9, 0x4fb8, {0xbe, 0x21, 0x57, 0xa3, 0xef, 0x2b, 0x62, 0x6c}
};
static const IID IID_IAudioCaptureClient = {
    0xc8adbd64, 0xe71e, 0x48a0, {0xa4, 0xde, 0x18, 0x5c, 0x39, 0x5c, 0xd3, 0x17}
};
static const IID IID_ISimpleAudioVolume = {
    0x87ce5498, 0x68d6, 0x44e5,{ 0x92, 0x15, 0x6d, 0xa4, 0x7e, 0xf8, 0x83, 0xd8 }
};
static const GUID KSDATAFORMAT_SUBTYPE_IEEE_FLOAT = {0x00000003,0x0000,0x0010, {0x80, 0x00, 0x00, 0xaa, 0x00, 0x38, 0x9b, 0x71}};
static const GUID KSDATAFORMAT_SUBTYPE_PCM = {0x00000001,0x0000,0x0010, {0x80, 0x00, 0x00, 0xaa, 0x00, 0x38, 0x9b, 0x71}};
#endif

typedef struct {
	int32_t windowWidth, windowHeight, framerate, frameDelay;
	list shaders, options;
} settings;

#define settings_fieldCount 6
#define REFLECT_TARGET settings
const reflection_data settings_fields[settings_fieldCount] = {REFLECTF(int32_t,windowWidth),REFLECTF(int32_t,windowHeight),REFLECTF(int32_t,framerate),REFLECTF(int32_t,frameDelay),REFLECTF(list,shaders),REFLECTF(list,options)};
#undef REFLECT_TARGET

char cinput[256];
bool running = true, hasConsoleInput = false;

void readConsoleInput() {
	while (running) {
		if (hasConsoleInput) threadSleep(100);
		else {
			fgets(cinput, 256, stdin);
			hasConsoleInput = true;
		}
	}
}

typedef struct {
	texture *texture;
	ubp_memory ubm;
	union {
		GLuint blockIndex;
		GLint textureIndex;
	};
} shaderoption;

typedef struct {
	shader *shader;
	list options;
	GLuint shaderviewUBI,audioUBI;
	GLint bufferIndex[32];
} loadedshader;

typedef struct {
	vec4 mouse;
	vec2 screen;
	GLint frame,framerate;
} shaderviewblock;

void buildUniformDataSize(list *jv, uint32_t *sz) {
	const uint32_t l = jv->length;
	for (uint32_t i = 0; i < l; i++) {
		json *v = (json*)jv->buffer + i;
		const uint8_t t = v->type;
		if (t >= JSON_TYPE_INT) sz[0]++;
		else {
			if (t != JSON_TYPE_STRING) {
				list *nl = NULL;
				if (t == JSON_TYPE_OBJECT) nl = &v->objectv->values;
				else if (t == JSON_TYPE_ARRAY) nl = v->listv;
				buildUniformDataSize(nl, sz);
			}
		}
	}
}
void buildUniformData(list *jv, uint32_t *dat, uint32_t ind) {
	const uint32_t l = jv->length;
	for (uint32_t i = 0; i < l; i++) {
		json *v = (json*)jv->buffer + i;
		const uint8_t t = v->type;
		if (t >= JSON_TYPE_INT) *(uint32_t*)(dat + (ind++)) = v->intv;
		else {
			list *nl = NULL;
			if (t == JSON_TYPE_OBJECT) nl = &v->objectv->values;
			else if (t == JSON_TYPE_ARRAY) nl = v->listv;
			buildUniformData(nl, dat, ind);
		}
	}
}

void deleteShaderOption(shaderoption *sopt) {
	if (sopt->texture == NULL) {
		ubp_free(sopt->ubm);
	}
	else {
		delete_texture(sopt->texture);
		free(sopt->texture);
	}
}
void deleteShader(loadedshader *ls) {
	if (ls->shader == NULL) return;
	const uint32_t ol = ls->options.length;
	shaderoption *sopt = ls->options.buffer;
	for (uint32_t i = 0; i < ol; i++) deleteShaderOption(sopt++);
	delete_list(&ls->options);
	delete_shader(ls->shader);
	free(ls->shader);
}
void loadShaderOption(loadedshader *ls, json_object *ojs, json *jsp, list *nlp, const int32_t ni) {
	if (jsp->type == JSON_TYPE_STRING) {
		//texture data
		uint32_t tdim[3];
		char *tpcstr = list_as_cstring(jsp->listv);
		FILE *fp = fopen(tpcstr, FILE_MODE_READ);
		void *tdata = NULL;
		if (fp != NULL) {
			tdata = load_png(fp, &tdim[0], true);
			fclose(fp);
		}
		if (tdata != NULL) {
			//printf("Loaded %dx%d PNG with %d bytes per pixel.\n",
			texture *tex = malloc(sizeof(texture));
			new_texture(tex);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
			texture_setData(tex, tdata, tdim, true, false);
			tex->data = NULL;
			free(tdata);
			shaderoption sho;
			sho.texture = tex;
			sho.textureIndex = glGetUniformLocation(ls->shader->current.program, list_as_cstring(nlp));
			list_set(&ls->options, &sho, ni);
			list_clear_cstring(nlp);
		}
		else {
			printf("Failed to load texture '%s'.\n", tpcstr);
		}
		list_clear_cstring(jsp->listv);
	}
	else {
		//uniform data
		list *nl;
		if (jsp->type == JSON_TYPE_OBJECT) {
			jsp->objectv->parent = ojs;
			nl = &jsp->objectv->values;
		}
		else nl = jsp->listv;
		uint32_t sz = 0;
		buildUniformDataSize(nl, &sz);
		if (sz != 0) {
			sz *= 4;
			uint32_t *udat = malloc(sz);
			buildUniformData(nl, udat, 0);
			shaderoption sho;
			sho.texture = NULL;
			sho.ubm = ubp_alloc(sz);
			ubp_memory_set(sho.ubm, udat, sz);
			free(udat);
			sho.blockIndex = glGetUniformBlockIndex(ls->shader->current.program, list_as_cstring(nlp));
			list_set(&ls->options, &sho, ni);
			list_clear_cstring(nlp);
		}
	}
}
uint32_t loadShader(list *shaderl, json_object *passOptions, const char *spcstr, const uint32_t i) {
	uint32_t ubuf = 0;
	uint64_t fsz;
	char *fd = file_readall(spcstr, &fsz);
	if (fd != NULL) {
		//add includes
		const char *incStr = "#include";
		int32_t incInd = memIndexOf(fd, 1, fsz, incStr, 8);
		if (incInd != -1) {
			int32_t fl = fsz, ninc = 0;
			list ctxt;
			new_list(&ctxt, 1);
			list_addm(&ctxt, fd, fl);
			while (incInd != -1) {
				uint8_t *tb = ctxt.buffer;
				char qstring = '"';
				int32_t oInd = incInd + 8,
					sInd = memIndexOf(tb + oInd, 1, fl - oInd, &qstring, 1);
				if (sInd == -1) break;
				int32_t tInd = oInd+sInd+1,
					eInd = memIndexOf(tb+tInd,1,fl-tInd,&qstring,1);
				if (eInd == -1) break;

				tb[tInd+eInd] = '\0';
				uint64_t tsz;
				char *ftxt = file_readall(tb+tInd,&tsz);
				if (ftxt != NULL) {
					list_removeRange(&ctxt,incInd,tInd+eInd+1-incInd);
					list_insertm(&ctxt,ftxt,incInd,tsz);
					free(ftxt);
					tb = ctxt.buffer;
					fl = ctxt.length;
					tInd = incInd+tsz;
					ninc++;
				} else {
					free(ftxt);
					tInd += eInd;
					printf("Failed to load shader include '%s'.\n",tb+tInd);
					break;
				}

				incInd = memIndexOf(tb+tInd,1,fl-tInd,incStr,8);
				if (incInd != -1) incInd += tInd;
			}
			if (ninc != 0) {
				free(fd);
				fd = ctxt.buffer;
				fsz = ctxt.length;
			} else delete_list(&ctxt);
		}
	
		list fl = construct_list(1);
		list_addm(&fl,fd,fsz);
		shader *ns = malloc(sizeof(shader));
		if (new_shader(ns, fl)) {
			if (i < shaderl->length) deleteShader((loadedshader*)shaderl->buffer + i);
			loadedshader ls;
			ls.shader = ns;
			ls.shaderviewUBI = glGetUniformBlockIndex(ns->current.program, "ShaderviewBlock");
			ls.audioUBI = glGetUniformBlockIndex(ns->current.program, "AudioBlock");
			char bbuf[16];
			for (uint32_t k = 0; k < 32; k++) {
				snprintf(bbuf,16,"Buffer%d",k);
				ls.bufferIndex[k] = glGetUniformLocation(ns->current.program, bbuf);
				if (ls.bufferIndex[k] != -1) ubuf |= 1<<k;
			}
			new_list(&ls.options, sizeof(shaderoption));
			if (passOptions->values.length <= i) {
				json js = { malloc(sizeof(json_object)), JSON_TYPE_OBJECT };
				new_json_object(js.objectv);
				js.objectv->parent = passOptions;
				list_set(&passOptions->values, &js, i);
				const list nl = { NULL, 0, 1, 0 };
				list_set(&passOptions->keys, &nl, i);
			}
			else {
				json_object *ojs = ((json*)passOptions->values.buffer)[i].objectv;
				const uint32_t nso = ojs->values.length;
				json *jsp = ojs->values.buffer;
				list *nlp = ojs->keys.buffer;
				for (uint32_t k = 0; k < nso; k++) loadShaderOption(&ls, ojs, jsp++, nlp++, ls.options.length);
			}
			list_set(shaderl, &ls, i);
		}
		else {
			free(ns);
			loadedshader ls;
			ls.shader = NULL;
			list_set(shaderl,&ls,i);
			if (passOptions->values.length <= i) {
				json js = { malloc(sizeof(json_object)), JSON_TYPE_OBJECT };
				new_json_object(js.objectv);
				js.objectv->parent = passOptions;
				list_set(&passOptions->values, &js, i);
				const list nl = { NULL, 0, 1, 0 };
				list_set(&passOptions->keys, &nl, i);
			}
		}
		delete_list(&fl);
		free(fd);
	}
	else {
		printf("Could not find shader file '%s'.\n", spcstr);
	}
	return ubuf;
}
void loadOptions(json_object *passOptions, json_object *pjs, list *shaderl, const uint32_t passId) {
	if (pjs->values.length > 0) {
		json_object *ojs = ((json*)passOptions->values.buffer)[passId].data;
		loadedshader *lsh = (loadedshader*)shaderl->buffer + passId;
		json *jsp = pjs->values.buffer;
		list *nlp = pjs->keys.buffer;
		for (uint32_t i = 0; i < pjs->values.length; i++) {
			bool removal = jsp->type == JSON_TYPE_INT && jsp->intv == 0;
			if (jsp->type < JSON_TYPE_INT || removal) {
				int32_t ni = json_object_getIndexOf(ojs, nlp);
				if (ni == -1) ni = ojs->values.length;
				else {
					delete_json((json*)ojs->values.buffer + ni);
					delete_list((list*)ojs->keys.buffer + ni);
					deleteShaderOption((shaderoption*)lsh->options.buffer + ni);
					if (removal) {
						list_removeAt(&ojs->values, ni);
						list_removeAt(&ojs->keys, ni);
						list_removeAt(&lsh->options, ni);
						delete_json(jsp);
						delete_list(nlp);
						jsp++;
						nlp++;
						continue;
					}
				}
				loadShaderOption(lsh, ojs, jsp, nlp, ni);
				list_set(&ojs->values, jsp, ni);
				list_set(&ojs->keys, nlp, ni);
			}
			else {
				delete_json(jsp);
				delete_list(nlp);
			}
			jsp++;
			nlp++;
		}
		pjs->values.length = pjs->keys.length = 0;
	}
	delete_json_object(pjs);
	free(pjs);
}

void updateRenderBuffers(list *renderBuffers, list *shaderPaths, const uint32_t obuf, const uint32_t ubuf, const uint32_t wndWidth, const uint32_t wndHeight) {
	uint32_t nl = shaderPaths->length*2, ol = renderBuffers->length;

	ol = min(ol,32);
	for (uint32_t i = 0; i < ol; i++) {
		if ((obuf>>i)&1) {
			rendertexture *rp = renderBuffers->buffer;
			rp += i*2;
			delete_rendertexture(rp++);
			delete_rendertexture(rp);
		}
	}
		
	renderBuffers->length = nl;
	list_expand(renderBuffers);
	nl /= 2;
	nl = min(nl,32);
	for (uint32_t i = 0; i < nl; i++) {
		if ((ubuf>>i)&1) {
			rendertexture *rp = renderBuffers->buffer;
			rp += i*2;
			new_rendertexture(rp++,wndWidth,wndHeight,GL_RGBA32F,24,true,false,0);
			new_rendertexture(rp,wndWidth,wndHeight,GL_RGBA32F,24,true,false,0);
		}
	}
}
void resizeRenderBuffers(list *renderBuffers, const uint32_t ubuf, const uint32_t wndWidth, const uint32_t wndHeight) {
	uint32_t nl = renderBuffers->length/2;
	nl = min(nl,32);
	for (uint32_t i = 0; i < nl; i++) {
		if ((ubuf>>i)&1) {
			rendertexture *rp = renderBuffers->buffer;
			rp += i*2;
			delete_rendertexture(rp);
			new_rendertexture(rp++,wndWidth,wndHeight,GL_RGBA32F,24,true,false,0);
			delete_rendertexture(rp);
			new_rendertexture(rp,wndWidth,wndHeight,GL_RGBA32F,24,true,false,0);
		}
	}
}

#ifdef __linux__
int main(int argc, char **argv) {//linux entry point
	parseCommandLineArguments(argc, argv);
#else
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR lpCmdLine, int nShowCmd) {//windows entry point
	parseCommandLineArguments();
	if (!(argumentCount != 0 && strcmp(arguments[0],"noconsole") == 0)) setupWindowsConsole(true);
#endif
	//load settings
	settings cfg;
	json_object *passOptions;
	list shaderPaths;
	new_list(&shaderPaths, sizeof(list));
	stream sfs;
	const char *settingsfp = "lastsettings";
	if (new_file_stream(&sfs, settingsfp, FILE_MODE_READ)) {
		stream_deserializeFixed(&sfs, &cfg, settings_fields, settings_fieldCount);
		file_stream_close(&sfs);
		passOptions = json_parse(&cfg.options).objectv;
		list spaths = string_split(cfg.shaders.buffer, cfg.shaders.length, ',', true);
		list_addm(&shaderPaths, spaths.buffer, spaths.length);
		delete_list(&spaths);
	}
	else {
		//default
		cfg.windowWidth = 800;
		cfg.windowHeight = 600;
		cfg.framerate = 50;
		cfg.frameDelay = 0;
		new_list(&cfg.shaders, 1);
		new_list(&cfg.options, 1);
		list_addm(&cfg.options, "{}", 2);
		passOptions = malloc(sizeof(json_object));
		new_json_object(passOptions);
	}

	//init SDL, window and opengl context
	SDL_Init(SDL_INIT_VIDEO);
	window wnd;
	wnd.vsync = false;
	if (!new_window(&wnd, "Shaderview", cfg.windowWidth, cfg.windowHeight, true, false, false)) return 0;
	window_init_graphics(wnd);

	int minVer = 0,maxVer = 0,profile = 0;
	SDL_GL_GetAttribute(SDL_GL_CONTEXT_MINOR_VERSION,&minVer);
	SDL_GL_GetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION,&maxVer);
	SDL_GL_GetAttribute(SDL_GL_CONTEXT_PROFILE_MASK,&profile);
	printf("Running OpenGL %d.%d %s profile.\nShaderview - Type help for more information.\n",maxVer,minVer,profile==SDL_GL_CONTEXT_PROFILE_CORE?"core":"compatibility");

	//start seperate thread to listen for console input
	createThread(&readConsoleInput, NULL, 0);

	//initial options & data
	uint32_t useBuffer = 0;
	list renderBuffers;
	new_list(&renderBuffers,sizeof(rendertexture));
	
	list shaderl;
	new_list(&shaderl, sizeof(loadedshader));
	for (uint32_t i = 0; i < shaderPaths.length; i++) {
		list *spl = (list*)shaderPaths.buffer + i;
		char *spcstr = list_as_cstring(spl);
		useBuffer |= loadShader(&shaderl, passOptions, spcstr, i);
		list_clear_cstring(spl);
	}
	updateRenderBuffers(&renderBuffers,&shaderPaths,0,useBuffer,wnd.width,wnd.height);

	shaderviewblock svblock;
	svblock.mouse = (vec4){ 0.0f, 0.0f, 0.0f, 0.0f };
	svblock.screen = (vec2){ wnd.widthf, wnd.heightf };
	svblock.frame = 0;
	svblock.framerate = cfg.framerate;
	ubp_memory svblockUBM = ubp_alloc(sizeof(shaderviewblock)), audioblockUBM;
	
	#ifndef __linux__
	IMMDevice *immDevice;
	IAudioClient *audioClient;
    IAudioCaptureClient *captureClient;
    uint32_t audioBufferIndex, audioBufferSize, audioSamples, audioChannels, audioFloat;
    #define FREQSZ 64
    float audioFd[FREQSZ*4], audioBuffer[FREQSZ], *audioDb;
    bool audioUpdated = false;
	#endif
	
	FILE *ffmpeg;
	uint8_t *capturePixels = NULL, *captureRows = NULL;
	uint32_t videoFrame, videoFrames;
	bool capture = false, audioInitialized = false;

	//initial graphics options
	setCullFace(CULLFACE_BACK);
	setDepthTest(DEPTHTEST_NONE);
	enableDepthWrite(false);
	setBlendMode(BLENDMODE_NONE);

	//render loop
	SDL_Event event;
	bool rendering = true, renderOnce = false;
	uint32_t framerateInterval = 1000 / cfg.framerate;
	START_SYNC_HERTZ();
	while (running) {
		//framerate timing
		if (capture) {
			hsyncTime = timeNow();
		}
		else {
			SYNC_HERTZ(framerateInterval);
		}

		//rendering
		if (wnd.active && (rendering || renderOnce || capture)) {
			ubp_memory_set(svblockUBM, &svblock, sizeof(shaderviewblock));
			svblock.frame++;
			
			mesh_bind(&quadMesh);
			loadedshader *sb = shaderl.buffer;
			for (uint32_t s = 0; s < shaderl.length; s++) {
				if (sb->shader == NULL) {
					sb++;
					continue;
				}
				shader_bind(sb->shader);
				bool urbuf = s<32?((useBuffer>>s)&1):false;
				rendertexture *rt = renderBuffers.buffer;
				if (urbuf) rendertexture_bind(rt+s*2+svblock.frame%2,0.0f,0.0f,1.0f,1.0f);
				else rendertexture_bind(&screenRenderTexture,0.0f,0.0f,1.0f,1.0f);
				for (uint32_t k = 0; k < shaderl.length; k++) {
					if (sb->bufferIndex[k] != -1) texture_bind(&(rt+k*2+(k<s?svblock.frame%2:(svblock.frame+1)%2))->color,sb->bufferIndex[k]);
				}
				ubp_memory_bind(svblockUBM, sb->shaderviewUBI);
				#ifndef __linux__
				if (sb->audioUBI != GL_INVALID_INDEX) {
					if (!audioInitialized) {
						printf("Initializing audio loopback...\n");
						#define aerr if (FAILED(hr)) {printf("Failed to initialize audio loopback.\n");goto finerr;}
					    IMMDeviceEnumerator *pEnumerator = NULL;
					    HRESULT hr = CoCreateInstance(&CLSID_MMDeviceEnumerator, NULL, CLSCTX_ALL, &IID_IMMDeviceEnumerator, &pEnumerator);
					    aerr;
						hr = pEnumerator->lpVtbl->GetDefaultAudioEndpoint(pEnumerator, eRender, eConsole, &immDevice);
						aerr;
						hr = immDevice->lpVtbl->Activate(immDevice,&IID_IAudioClient, CLSCTX_ALL, NULL, &audioClient);
						aerr;
						WAVEFORMATEX *format = NULL;
						hr = audioClient->lpVtbl->GetMixFormat(audioClient,&format);
						aerr;
						audioChannels = format->nChannels;
						#define nsf printf("Unsupported audio format.\n");goto finerr
						if (format->wFormatTag == WAVE_FORMAT_PCM) audioFloat = false;
						else if (format->wFormatTag == WAVE_FORMAT_IEEE_FLOAT) audioFloat = true;
						else if (format->wFormatTag == WAVE_FORMAT_EXTENSIBLE) {
							WAVEFORMATEXTENSIBLE *formatx = format;
							if (memcmp(&formatx->SubFormat,&KSDATAFORMAT_SUBTYPE_PCM,sizeof(GUID)) == 0) audioFloat = false;
							else if (memcmp(&formatx->SubFormat,&KSDATAFORMAT_SUBTYPE_IEEE_FLOAT,sizeof(GUID)) == 0) audioFloat = true;
							else {
								nsf;
							}
						} else {
							nsf;
						}
						if (format->wBitsPerSample != (audioFloat?32:16)) {
							nsf;
						}
						#undef nsf
						hr = audioClient->lpVtbl->Initialize(audioClient,AUDCLNT_SHAREMODE_SHARED,AUDCLNT_STREAMFLAGS_LOOPBACK,20*10000,0,format,NULL);
   						aerr;
   						hr = audioClient->lpVtbl->GetBufferSize(audioClient,&audioSamples);
						aerr;
						audioBufferSize = format->nSamplesPerSec/50;
						audioDb = malloc(audioBufferSize*4);
						audioBufferIndex = 0;
						hr = audioClient->lpVtbl->GetService(audioClient,&IID_IAudioCaptureClient,&captureClient);
						aerr;
						hr = audioClient->lpVtbl->Start(audioClient);
						aerr;
						#undef aerr
						for (uint32_t i = 0; i < FREQSZ*4; i++) audioFd[i] = 0.0f;
						printf("Initialized %s audio with buffer of %d, frequency of %d and %d channels\n",audioFloat?"floating point":"PCM",audioSamples,audioBufferSize,format->nSamplesPerSec,format->nChannels);
						
						audioblockUBM = ubp_alloc(FREQSZ*16);
						audioInitialized = true;
					}
					
					if (!audioUpdated) {
						uint8_t *audiod;
						uint32_t flags = 0,nAudioFrames = 0;
						if (!FAILED(captureClient->lpVtbl->GetBuffer(captureClient,&audiod,&nAudioFrames,&flags,NULL,NULL))) {
							if (flags&AUDCLNT_BUFFERFLAGS_SILENT) {
								for (uint32_t i = 0; i < FREQSZ*4; i++) audioFd[i] *= 0.5f;
								ubp_memory_set(audioblockUBM,audioFd,FREQSZ*16);
								audioBufferIndex = 0;
							} else {
								float *op = audioDb+audioBufferIndex;
								const float cscale = 1.0f/(float)audioChannels;
								uint32_t samples = nAudioFrames,samplesLeft;
								while (samples != 0) {
									samplesLeft = audioBufferSize-audioBufferIndex;
									if (samplesLeft > samples) samplesLeft = samples;
									if (audioFloat) {
										float *fad = audiod;
										for (uint32_t ai = 0; ai < samplesLeft; ai++) {
											float v = 0.0f;
											for (uint32_t tc = 0; tc < audioChannels; tc++) v += *fad++;
											*op++ = v;
										}
										audiod = fad;
									} else {
										int16_t *iad = audiod;
										for (uint32_t ai = 0; ai < samplesLeft; ai++) {
											float v = 0.0f;
											for (uint32_t tc = 0; tc < audioChannels; tc++) v += (*iad++)/(float)SHRT_MAX;
											*op++ = v;
										}
										audiod = iad;
									}
									audioBufferIndex += samplesLeft;
									if (audioBufferIndex >= audioBufferSize) {
										uint32_t lpos = 0;
										for (uint32_t ms = 0; ms < FREQSZ; ms++) {
											uint32_t npos = (ms+1)*audioBufferSize/FREQSZ;
											if (npos > audioBufferSize) npos = audioBufferSize;
											float v = 0.0f;
											for (uint32_t i = lpos; i < npos; i++) v += audioDb[i];
											audioBuffer[ms] = v*0.5f/(float)(npos-lpos);
											lpos = npos;
										}
										cosinetransform(audioBuffer,audioBuffer,FREQSZ,false);
										float *afd = audioFd;
										for (uint32_t i = 0; i < FREQSZ; i++) {
											*afd = (*afd)*0.8f+0.2f*log10f(1.0f+fabsf(audioBuffer[i])*(1.0f+i)*10.0f);
											afd += 4;
										}
										for (uint32_t b = 0; b < 3; b++) {
											float *sfd = audioFd+b, *dfd = sfd+1;
											*dfd = (sfd[0]+sfd[4])*0.5f;
											dfd += 4;
											uint32_t mii = 0, mai = 3;
											for (uint32_t i = 1; i < FREQSZ-1; i++) {
												float s = 0.0f;
												for (uint32_t si = mii; si < mai; si++) s += sfd[si*4];
												mii++;
												mai++;
												*dfd = s*0.333333333f;
												dfd += 4;
											}
											*dfd = (sfd[(FREQSZ-2)*4]+sfd[(FREQSZ-1)*4])*0.5f;
											dfd += 4;
										}
										ubp_memory_set(audioblockUBM,audioFd,FREQSZ*16);
										audioBufferIndex = 0;
										op = audioDb;
									}
									samples -= samplesLeft;
								}
							}
						}
						captureClient->lpVtbl->ReleaseBuffer(captureClient,nAudioFrames);
					
						ubp_memory_bind(audioblockUBM,sb->audioUBI);
						goto fin;
					
						finerr:sb->audioUBI = GL_INVALID_INDEX;
						fin:;
					}
					audioUpdated = true;
				}
				#endif
				const uint32_t sol = sb->options.length;
				shaderoption *so = sb->options.buffer;
				for (uint32_t i = 0; i < sol; i++) {
					if (so->texture == NULL) ubp_memory_bind(so->ubm, so->blockIndex);
					else texture_bind(so->texture, so->textureIndex);
					so++;
				}
				glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
				so = sb->options.buffer;
				for (uint32_t i = 0; i < sol; i++) {
					if (so->texture == NULL) ubp_memory_unbind();
					else texture_unbind();
					so++;
				}
				ubp_memory_unbind();
				if (sb->audioUBI != GL_INVALID_INDEX) {
					ubp_memory_unbind();
				}
				for (uint32_t k = 0; k < shaderl.length; k++) {
					if (sb->bufferIndex[k] != -1) texture_unbind();
				}
				sb++;
			}

			if (cfg.frameDelay == 0 || svblock.frame%cfg.frameDelay == (cfg.frameDelay - 1)) {
				if (shaderl.length != 0 && shaderl.length < 33 && (useBuffer >> (shaderl.length - 1)) & 1) rendertexture_blit(activeRenderTexture, 0.0f, 0.0f, 1.0f, 1.0f, &screenRenderTexture, 0.0f, 0.0f, 1.0f, 1.0f);
				window_swapBuffers(&wnd);
				if (capture) {
					glReadPixels(0, 0, wnd.width, wnd.height, GL_RGBA, GL_UNSIGNED_BYTE, capturePixels);
					if (videoFrames == 0) {
						uint32_t dim[3];
						dim[0] = wnd.width;
						dim[1] = wnd.height;
						dim[2] = 4;
						char pbuf[128];
						snprintf(pbuf, 128, "capture%d.png", videoFrame);
						FILE *cout = fopen(pbuf, FILE_MODE_WRITE);
						save_png(cout, capturePixels, &dim[0], captureRows, true);
						fclose(cout);
						printf("Saved frame image as '%s'.\n", pbuf);
						free(capturePixels);
					} else {
						fwrite(capturePixels,1,wnd.width*wnd.height*4,ffmpeg);
						videoFrame++;
						if (videoFrame >= videoFrames) {
							pclose(ffmpeg);
							printf("Done capturing!.\n");
							capture = false;
							free(capturePixels);
						}
					}
				}
			}
			#ifndef __linux__
			audioUpdated = false;
			#endif
			renderOnce = false;
		}

		//mouse, window, keyboard events
		while (SDL_PollEvent(&event)) {
			switch (event.type) {
			case SDL_MOUSEMOTION:
				svblock.mouse.x = event.motion.x*wnd.oneDivWidth*2.0f - 1.0f;
				svblock.mouse.y = 1.0f - event.motion.y*wnd.oneDivHeight*2.0f;
				break;
			case SDL_MOUSEBUTTONDOWN:
				if (event.button.button == SDL_BUTTON_LEFT) svblock.mouse.z = 1.0f;
				else svblock.mouse.w = 1.0f;
				break;
			case SDL_MOUSEBUTTONUP:
				if (event.button.button == SDL_BUTTON_LEFT) svblock.mouse.z = 0.0f;
				else svblock.mouse.w = 0.0f;
				break;
			case SDL_WINDOWEVENT:
				if (event.window.event == SDL_WINDOWEVENT_MINIMIZED) wnd.active = false;
				else if (event.window.event == SDL_WINDOWEVENT_RESTORED) wnd.active = true;
				break;
			case SDL_QUIT:
				running = false;
				break;
			}
		}

		//process console input
		if (hasConsoleInput) {
			const list sHelp = cstring_as_list("h"), sWndWidth = cstring_as_list("windoww"), sWndHeight = cstring_as_list("windowh"),
				sFramerate = cstring_as_list("f"), sRender = cstring_as_list("r"), sShader = cstring_as_list("s"), sDelay = cstring_as_list("d"),
				sOptions = cstring_as_list("o"), sCapture = cstring_as_list("c"), sMaterial = cstring_as_list("m"), sStart = cstring_as_list("sta"), sStop = cstring_as_list("sto"), sUpdate = cstring_as_list("u");

			list il = cstring_as_list(cinput);
			if (il.length != 0) {
				il.length--;cinput[il.length] = '\0';//remove newline char from input
			}
			const char sc = ' ';
			bool settingsModified = false;
			int32_t argIndex = list_indexOf(&il, &sc, 1);
			string_applyCharacterModifier(il.buffer, argIndex == -1 ? il.length : argIndex, tolower);
			if (list_startsWith(&il, &sHelp)) {
				printf("Shaderview is a simple application to help you quickly view and develop shaders. It supports multiple passes, mouse input, allows capture of images/video and control over all of the shader uniforms.\n\n"
					"Commands:\n"
					"help - Lists information about Shaderview.\n"
					"windowWidth value - Get/set window width. Example: 'windowWidth 1280'.\n"
					"windowHeight value - Get/set window height.\n"
					"framerate value - Get/set framerate.\n"
					"delay value - Get/set rendering delay in frames.\n"
					"render start/pause/stop/once - Start/pause/stop rendering or render once(a single frame).\n"
					"shader id path - Get/set shader, id is pass index. If path is empty then pass id is removed.\n"
					"options id json_data - Set options(uniform data and textures), id is pass index. If json_data is zero/empty then options are removed.\n"
					"material id path - Load material uniform data and textures, id is pass index.\n"
					"capture videoDuration - Capture image/video starting at current frame.\n"
					"update - Re-load all shaders and restart from frame 0."
					"\n");
			}
			else if (list_startsWith(&il, &sWndWidth)) {
				if (argIndex != -1) {
					int32_t nv = atoi(cinput + argIndex + 1);
					cfg.windowWidth = max(nv, 1);
					window_setSize(&wnd, cfg.windowWidth, cfg.windowHeight, true, false);
					svblock.screen = (vec2){ wnd.widthf, wnd.heightf };
					resizeRenderBuffers(&renderBuffers,useBuffer,wnd.width,wnd.height);
					settingsModified = true;
				}
				printf("Window width is %d.\n", cfg.windowWidth);
			}
			else if (list_startsWith(&il, &sWndHeight)) {
				if (argIndex != -1) {
					int32_t nv = atoi(cinput + argIndex + 1);
					cfg.windowHeight = max(nv, 1);
					window_setSize(&wnd, cfg.windowWidth, cfg.windowHeight, true, false);
					svblock.screen = (vec2){ wnd.widthf, wnd.heightf };
					resizeRenderBuffers(&renderBuffers,useBuffer,wnd.width,wnd.height);
					settingsModified = true;
				}
				printf("Window height is %d.\n", cfg.windowHeight);
			}
			else if (list_startsWith(&il, &sFramerate)) {
				if (argIndex != -1) {
					int32_t nv = atoi(cinput + argIndex + 1);
					cfg.framerate = max(nv, 1);
					svblock.framerate = cfg.framerate;
					framerateInterval = 1000 / cfg.framerate;
					settingsModified = true;
				}
				printf("Framerate is set to %d, time offset is %d.\n", cfg.framerate, hdeltaTime);
			}
			else if (list_startsWith(&il, &sDelay)) {
				if (argIndex != -1) {
					cfg.frameDelay = atoi(cinput + argIndex + 1);
					settingsModified = true;
				}
				printf("Frame delay is set to %d.\n", cfg.frameDelay);
			}
			else if (list_startsWith(&il, &sRender)) {
				if (argIndex != -1) {
					list as;
					as.stride = 1;
					argIndex++;
					list_setBuffer(&as, cinput + argIndex, il.length - argIndex);
					if (list_startsWith(&as, &sStart)) rendering = true;
					else if (list_startsWith(&as, &sStop)) {
						rendering = false;
						svblock.frame = 0;
					}
					else if (list_startsWith(&as, &sOptions)) renderOnce = true;
					else rendering = false;
				}
				printf("Rendering: %s, Frame: %d.\n", renderOnce ? "Once" : (rendering ? "On" : "Off"), svblock.frame);
			}
			else if (list_startsWith(&il, &sShader)) {
				if (argIndex != -1) {
					argIndex++;
					int32_t pathIndex = list_indexOfI(&il, &sc, 1, argIndex),passId;
					bool set = pathIndex!=-1;
					if (set) cinput[pathIndex] = '\0';
					passId = atoi(cinput + argIndex);
					if (set) {
						pathIndex++;
						if (il.length == pathIndex) {
							//delete
							if (shaderl.length > passId) {
								deleteShader((loadedshader*)shaderl.buffer + passId);
								list_removeAt(&shaderl, passId);
								delete_json((json*)passOptions->values.buffer + passId);
								list_removeAt(&passOptions->values, passId);
								list_removeAt(&passOptions->keys, passId);
							}
							if (shaderPaths.length > passId) list_removeAt(&shaderPaths, passId);
						}
						else {
							//add/set
							list spstr;
							new_list(&spstr, 1);
							list_addm(&spstr, cinput + pathIndex, il.length - pathIndex);
							if (passId < shaderPaths.length) delete_list((list*)shaderPaths.buffer + passId);
							list_set(&shaderPaths, &spstr, passId);
							uint32_t oubuf = useBuffer;
							useBuffer |= loadShader(&shaderl, passOptions, cinput + pathIndex, passId);
							updateRenderBuffers(&renderBuffers,&shaderPaths,oubuf,useBuffer,wnd.width,wnd.height);
						}
						cfg.shaders.length = 0;
						list *spl = shaderPaths.buffer;
						for (uint32_t i = 0; i < shaderPaths.length; i++) {
							if (i != 0) {
								const char comc = ',';
								list_add(&cfg.shaders, &comc);
							}
							list_addm(&cfg.shaders, spl->buffer, spl->length);
							spl++;
						}
						settingsModified = true;
					}
					if (passId < shaderPaths.length) {
						list *sn = (list*)shaderPaths.buffer + passId;
						printf("Pass %d is using shader '%.*s'.\n", passId, sn->length, sn->buffer);
					}
				}
			}
			else if (list_startsWith(&il, &sOptions)) {
				if (argIndex != -1) {
					argIndex++;
					int32_t dataIndex = list_indexOfI(&il, &sc, 1, argIndex);
					if (dataIndex != -1) {
						cinput[dataIndex] = '\0';
						dataIndex++;
						int32_t passId = atoi(cinput + argIndex);
						if (passId < passOptions->values.length) {
							list nl;
							nl.stride = 1;
							list_setBuffer(&nl, cinput + dataIndex, il.length-dataIndex);
							json_object *pjs = json_parse(&nl).objectv;
							loadOptions(passOptions, pjs, &shaderl, passId);
							cfg.options.length = 0;
							json_stringify(passOptions, &cfg.options, false, false);
							settingsModified = true;
						}
					}
				}
				printf("Options: %.*s\n\n", cfg.options.length, cfg.options.buffer);
			}
			else if (list_startsWith(&il, &sMaterial)) {
				if (argIndex != -1) {
					argIndex++;
					int32_t dataIndex = list_indexOfI(&il, &sc, 1, argIndex);
					if (dataIndex != -1) {
						cinput[dataIndex] = '\0';
						dataIndex++;
						int32_t passId = atoi(cinput + argIndex);
						if (passId < passOptions->values.length) {
							uint64_t fsz;
							void *mdata = file_readall(cinput + dataIndex,&fsz);
							if (mdata != NULL) {
								list nl;
								nl.stride = 1;
								list_setBuffer(&nl, mdata, fsz);
								json_object *pjs = json_parse(&nl).objectv;
								const list tlstr = cstring_as_list("t");
								json *ojs = json_object_getClosest(pjs, &tlstr);
								if (ojs != NULL && ojs->type == JSON_TYPE_OBJECT) {
									loadOptions(passOptions, ojs->objectv, &shaderl, passId);
									ojs->type = JSON_TYPE_INT;
								}
								const list olstr = cstring_as_list("d");
								ojs = json_object_getClosest(pjs, &olstr);
								if (ojs != NULL && ojs->type == JSON_TYPE_OBJECT) {
									json_object *njo = malloc(sizeof(json_object));
									new_json_object(njo);
									const list mblstr = cstring_as_list("MaterialBlock");
									list_add(&njo->values, ojs);
									list_add(&njo->keys, &mblstr);
									loadOptions(passOptions, njo, &shaderl, passId);
								}
								delete_json_object(pjs);
								free(pjs);
								cfg.options.length = 0;
								json_stringify(passOptions, &cfg.options, false, false);
								settingsModified = true;
							}
						}
					}
				}
				printf("Options: %.*s\n\n", cfg.options.length, cfg.options.buffer);
			}
			else if (list_startsWith(&il, &sCapture)) {
				if (argIndex != -1) videoFrames = atoi(cinput + argIndex + 1);
				else videoFrames = 0;
				videoFrame = 0;
				if (videoFrames == 0) {
					printf("Capturing image...\n");
				}
				else {
					uint8_t pbuf[256];
					snprintf(pbuf, 256, "ffmpeg -loglevel quiet -nostdin -y -pix_fmt rgba -s %dx%d -r %d -f rawvideo -i pipe:0 -vcodec libx264 -pix_fmt yuv420p -frames:v %d -vf vflip video.mp4", cfg.windowWidth, cfg.windowHeight, cfg.framerate, videoFrames);
					ffmpeg = popen(pbuf,"wb");
					if (ffmpeg == NULL) {
						printf("Couldn't capture video, failed to open ffmpeg.exe.\n");
						videoFrames = 0;
					} else printf("Capturing %d frames of video at %d frames per second...\n", videoFrames, cfg.framerate);
				}
				
				uint32_t pxb = wnd.width*wnd.height * 4;
				capturePixels = malloc(pxb+sizeof(void*)*wnd.height);
				captureRows = capturePixels + pxb;
				capture = true;
			} else if (list_startsWith(&il,&sUpdate)) {
				svblock.frame = 0;
				list *spath = shaderPaths.buffer;
				uint32_t oubuf = useBuffer;
				for (uint32_t i = 0; i < shaderl.length; i++) {
					char *cstr = list_as_cstring(spath);
					printf("Re-loading shader '%s' of pass %d.\n", cstr, i);
					useBuffer |= loadShader(&shaderl, passOptions, cstr, i);
					list_clear_cstring(spath);
					spath++;
				}
				updateRenderBuffers(&renderBuffers,&shaderPaths,oubuf,useBuffer,wnd.width,wnd.height);
			} else {
				printf("Unknown command, try 'help' for a list of commands.\n");
			}

			if (settingsModified) {
				//save settings
				new_file_stream(&sfs, settingsfp, FILE_MODE_WRITE);
				stream_serializeFixed(&sfs, &cfg, settings_fields, settings_fieldCount);
				file_stream_close(&sfs);
			}
			hasConsoleInput = false;
		}
	}

	//cleanup
	rendertexture *rtex = renderBuffers.buffer;
	loadedshader *sb = shaderl.buffer;
	list *sp = shaderPaths.buffer;
	for (uint32_t s = 0; s < shaderl.length; s++) {
		if ((useBuffer>>s)&1) {
			delete_rendertexture(rtex++);
			delete_rendertexture(rtex++);
		} else rtex += 2;
		deleteShader(sb++);
		delete_list(sp++);
	}

	free_graphics();
	delete_window(&wnd);
	SDL_Quit();
	
	delete_list(&cfg.shaders);
	delete_list(&cfg.options);
	delete_list(&shaderPaths);
	delete_list(&shaderl);
	delete_list(&renderBuffers);
	delete_json_object(passOptions);
	free(passOptions);

	return 0;
}
